var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ProductSchema   = new Schema({
    id: Number,
    producer: String,
    model: String,
    code: String,
    dimension: Number,
    resolution: String,
    storage: String,
    memory: String,
    processor: String,
    graphicsCard: String,
    os: String,
    amount: Number,
    price: Number,
    description: String,
    rating: Number,
    imageUrl: String
});

module.exports = mongoose.model('Product', ProductSchema);