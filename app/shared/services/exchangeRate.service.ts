import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ExchangeRateService {
    private _resourceUrl: string = 'https://api.fixer.io/latest?base=USD&symbols=PLN';
    
    constructor(private _http: Http) {
        
    }
    
    getCurrentExchangeRate(): Observable<number> {
        return this._http.get(this._resourceUrl).
            map((response: Response) => <number>response.json().rates.PLN)
            .do(data => console.log('Current exchange rate: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Exchange rate server error');
    }
}