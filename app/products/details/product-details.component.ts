import { Component, OnInit } from '@angular/core';
import { RouteParams, Router } from '@angular/router-deprecated';

import { IProduct } from '../product';
import { ProductService } from '../product.service';
import { StarsComponent } from '../../shared/star_component/stars.component';
import { ProductPricePipe } from '../pipes/product-price.pipe';
import { ExchangeRateService } from '../../shared/services/exchangeRate.service';

@Component({
    templateUrl: 'app/products/details/product-details.component.html',
    styleUrls: ['app/products/details/product-details.component.css'],
    directives: [StarsComponent],
    pipes: [ProductPricePipe]
})
export class ProductDetailsComponent implements OnInit {
    pageTitle: string = 'Szczegółowe informacje o produkcie';
    product: IProduct;
    exchangeRate: number;

    constructor(private _routeParams: RouteParams, private _router: Router, 
    private _productService: ProductService, private _exchangeRateService: ExchangeRateService) {

    }

    ngOnInit(): void {
        this._exchangeRateService.getCurrentExchangeRate().subscribe(
            exchangeRate => this.exchangeRate = exchangeRate);

        let id = this._routeParams.get('id');
        this._productService.getProduct(id).
            subscribe(product => this.product = product);

    }

    onBack(): void {
        this._router.navigate(['Products']);
    }
}