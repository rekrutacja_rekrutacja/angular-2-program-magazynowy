import { Component } from '@angular/core';
import { Router } from '@angular/router-deprecated';

import { ProductService } from '../product.service';
import { IProduct } from '../product';

@Component({
    templateUrl: 'app/products/add_form/add-product.component.html',
    styleUrls: ['app/products/add_form/add-product.component.css']
})
export class AddProductComponent {

    product: any = {};
    codePrefix: String = '';
    minPrice: number = 100.00;
    systems = ['Brak', 'Windows 7 HP', 'Windows 7 Pro', 'Windows 7 Ultimate', 'Windows 8',
        'Windows 8 Pro', 'Windows 8.1', 'Windows 8.1 Pro', 'Windows 10', 'Ubuntu Linux 16.04',
        'Manjaro Linux 16.06', 'OS X El Capitan', 'OS X Yosemite'];

    constructor(private _router: Router, private _productService: ProductService) {
        this.product.os = this.systems[0];
    }

    producerChanged(): void {
        if (this.product.producer) {
            if (this.product.producer.length <= 3) {
                this.codePrefix = this.product.producer.toUpperCase();
                if (this.codePrefix.length === 1) {
                    this.codePrefix += '00';
                } else if (this.codePrefix.length === 2) {
                    this.codePrefix += '0';
                }
                this.codePrefix += '-';
            } else {
                this.codePrefix = this.product.producer.substring(0, 3).toUpperCase() + '-';
            }
        } else {
            this.codePrefix = '';
        }
    }

    cancel(event): void {
        event.preventDefault();
        event.stopPropagation();
        this._router.navigate(['Products']);
    }
    
    onSubmit(): void {
        this.product.code = this.codePrefix + this.product.code;
        if(this.product.imageUrl) {
            this.product.imageUrl = 'http://' + this.product.imageUrl;
        }
        this._productService.addProduct(<IProduct>this.product).
            subscribe((response) => {
                this._router.navigate(['Products']);
            }, err => alert('Wystąpił błąd!'));
    }
}