import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { IProduct } from './product';

@Injectable()
export class ProductService {
    private _productUrl: string = 'http://localhost:5000/api/products/';

    constructor(private _http: Http) {

    }

    getProducts(): Observable<IProduct[]> {
        return this._http.get(this._productUrl)
            .map((response: Response) => <IProduct[]>response.json())
            .map((products: any) => {
                for (let product of products) {
                    product.id = product._id;
                }
                return products;
            })
            .do(data => console.log('Response: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getProduct(id: string): Observable<IProduct> {
        return this.getProducts()
            .map((products: IProduct[]) => products.find(p => p.id === id))
            .do(data => console.log('Response: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    deleteProduct(id: string): Observable<Response> {
        return this._http.delete(this._productUrl + id);
    }

    addProduct(product: IProduct): Observable<Response> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http.post(this._productUrl, JSON.stringify(product), {
            headers: headers
        });
    }
}