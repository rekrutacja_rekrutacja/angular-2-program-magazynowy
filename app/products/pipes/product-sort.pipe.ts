import { Pipe, PipeTransform } from '@angular/core';
import { IProduct } from '../product';

export enum Method {
    Lex,
    Num
}

export enum Order {
    Asc,
    Desc
}

@Pipe({
    name: 'productSort'
})
export class ProductSortPipe implements PipeTransform {
    
        private changeNumericalOrder(products: IProduct[], property: string, order: Order): IProduct[] {
        if (order == Order.Asc) {
            products = products.sort((p1, p2) => p1[property] - p2[property]);
        } else {
            products = products.sort((p1, p2) => p2[property] - p1[property]);
        }
        return products;
    }

    private changeLexicalOrder(products: IProduct[], property: string, order: Order): IProduct[] {
        if (order == Order.Asc) {
            products = products.sort((p1, p2) => {
                if (p1[property].toLowerCase() > p2[property].toLowerCase()) {
                    return 1;
                }
                if (p1[property].toLowerCase() < p2[property].toLowerCase()) {
                    return -1;
                }
                return 0;
            });
        } else {
            products = products.sort((p1, p2) => {
                if (p1[property].toLowerCase() > p2[property].toLowerCase()) {
                    return -1;
                }
                if (p1[property].toLowerCase() < p2[property].toLowerCase()) {
                    return 1;
                }
                return 0;
            });
        }
        return products;
    }
    
    transform(products: IProduct[], method: Method, property: string, order: Order): IProduct[] {
        if (method == Method.Lex) {
            products = this.changeLexicalOrder(products, property, order);
        }
        if (method == Method.Num) {
            products = this.changeNumericalOrder(products, property, order);
        }

        return products;
    }
}