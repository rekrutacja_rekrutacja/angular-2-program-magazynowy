import { Pipe, PipeTransform } from '@angular/core';
import { IProduct } from '../product';

@Pipe({
    name: 'productFilter'
})
export class ProductFilterPipe implements PipeTransform {
    transform(value: IProduct[], filterBy: string, filter: string): IProduct[] {
        filter = filter ? filter.toLowerCase() : null;

        let f = (product: IProduct) => {
            let s = product[filterBy];
            if (typeof s !== 'string') {
                s = s.toLocaleString();
            }
            s = s.toLocaleLowerCase();
            return s.indexOf(filter) != -1;
        }
        return filter ? value.filter(f) : value;
    }
}