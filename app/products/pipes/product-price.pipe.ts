import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'productPrice'
})
export class ProductPricePipe implements PipeTransform {
    
    transform(value: number, exchangeRate: number): number {
        return parseFloat((value * exchangeRate).toFixed(2));
    }
}