import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router-deprecated';

import { ProductPricePipe } from '../pipes/product-price.pipe';
import { IProduct } from '../product';
import { ProductFilterPipe } from '../pipes/product-filter.pipe';
import { StarsComponent } from '../../shared/star_component/stars.component';
import { ProductService } from '../product.service';
import { ExchangeRateService } from '../../shared/services/exchangeRate.service';
import { DataService } from '../../shared/services/data.service';
import { ProductSortPipe, Method, Order } from '../pipes/product-sort.pipe';

@Component({
    selector: 'warehouse-products',
    templateUrl: 'app/products/list/product-list.component.html',
    styleUrls: ['app/products/list/product-list.component.css'],
    pipes: [ProductPricePipe, ProductFilterPipe, ProductSortPipe],
    directives: [StarsComponent, ROUTER_DIRECTIVES]
})
export class ProductListComponent implements OnInit {

    pageTitle: string = 'Lista produktów';
    imageWidth: number = 100;
    imageMargin: number = 2;
    showImages: boolean = false;
    listFilter: string;
    filterBy: string;
    products: IProduct[];
    errorMessage: string;
    exchangeRate: number;

    sortColumn: string;
    sortMethod: Method;
    sortOrder: Order = Order.Desc;

    orders: Object = {
        producer: { order: Order.Desc, glyph: '' },
        model: { order: Order.Desc, glyph: '' },
        code: { order: Order.Desc, glyph: '' },
        amount: { order: Order.Desc, glyph: '' },
        price: { order: Order.Desc, glyph: '' },
        rating: { order: Order.Desc, glyph: '' },
    }

    constructor(private _productService: ProductService,
        private _exchangeRateService: ExchangeRateService,
        private _dataService: DataService) {
        _dataService.filterChanged.subscribe((value) => this.listFilter = value);
        _dataService.filterByChanged.subscribe((value) => this.filterBy = value);

    }

    ngOnInit(): void {
        this.listFilter = this._dataService.getFilter();
        this.filterBy = this._dataService.getFilterBy() || 'producer';

        this._productService.getProducts().subscribe(
            products => this.products = products,
            error => this.errorMessage = <any>error);

        this._exchangeRateService.getCurrentExchangeRate().subscribe(
            exchangeRate => this.exchangeRate = exchangeRate,
            error => this.errorMessage = <any>error);
    }

    toggleImages(): void {
        this.showImages = !this.showImages;
    }

    logChange(value: string): void {
        this.filterBy = value;
    }

    onRatingClicked(message: string): void {
        alert(message);
    }

    private setGlyph(property: string): void {
        for (let prop in this.orders) {
            if (prop !== property) {
                this.orders[prop].order = Order.Desc;
                this.orders[prop].glyph = '';
            }
        }
        if (this.orders[property].order === Order.Asc) {
            this.orders[property].glyph = '▲';
        } else {
            this.orders[property].glyph = '▼';
        }

    }

    changeSorting(prop: string, method: string) {
        if (this.orders[prop].order == Order.Asc) {
            this.orders[prop].order = Order.Desc;
        } else {
            this.orders[prop].order = Order.Asc;
        }

        this.setGlyph(prop);

        this.sortColumn = prop;
        this.sortMethod = method === 'lex' ? Method.Lex : Method.Num;
        this.sortOrder = this.orders[this.sortColumn].order;

        console.log(this.sortColumn + ' ' + this.sortMethod + ' ' + this.sortOrder);
    }

    delete(id: string): void {
        this._productService.deleteProduct(id).subscribe(
            (response) => {
                if (response.status === 200) {
                    for (let i = 0; i < this.products.length; i++) {
                        if (this.products[i].id === id) {
                            this.products.splice(i, 1);
                            break;
                        }
                    }
                }
            });
    }
}
