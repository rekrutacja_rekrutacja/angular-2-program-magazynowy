import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import 'rxjs/Rx';
import { RouteConfig, ROUTER_PROVIDERS, ROUTER_DIRECTIVES, Router } from '@angular/router-deprecated';

import { ProductListComponent } from './products/list/product-list.component';
import { ProductService } from './products/product.service';
import { ExchangeRateService } from './shared/services/exchangeRate.service';
import { ProductDetailsComponent } from './products/details/product-details.component';
import { HomeComponent } from './home/home.component';
import { DataService } from './shared/services/data.service';
import { AddProductComponent } from './products/add_form/add-product.component';


@Component({
	selector: 'warehouse-app',
	templateUrl: 'app/app.component.html',
	styles: ['.container { width: 90%; }'],
	directives: [ROUTER_DIRECTIVES],
	providers: [
		ProductService,
		HTTP_PROVIDERS,
		ExchangeRateService,
		ROUTER_PROVIDERS,
		DataService
	]
})
@RouteConfig([
	{ path: '/home', name: 'Home', component: HomeComponent, useAsDefault: true },
	{ path: '/products', name: 'Products', component: ProductListComponent },
	{ path: '/product/:id', name: 'ProductDetails', component: ProductDetailsComponent },
	{ path: '/addProduct', name: 'AddProduct', component: AddProductComponent }
])
export class AppComponent {

	filter: string;
	filterBy: string = 'producer';
	options: Array<[string, string]> = [
        ['producer', 'producent'], ['model', 'model'], ['code', 'kod produktu'],
        ['amount', 'liczba sztuk'], ['rating', 'ocena']
    ];

	constructor(private _router: Router, private _dataService: DataService) {

	}

	isActive(instruction: any[]): boolean {
		return this._router.isRouteActive(this._router.generate(instruction));
	}

	changeFilter(filter: string): void {
        this._dataService.setFilter(filter);
    }
	
	changeFilterBy(filterBy: string): void {
		this._dataService.setFilterBy(filterBy);
	}

}
